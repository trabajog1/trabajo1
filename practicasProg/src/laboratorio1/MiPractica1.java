package laboratorio1;

import java.util.Scanner;

public class MiPractica1 {
	private Rectangulo rectangulo1;
	private Rectangulo rectangulo2;
	private Rectangulo rectangulo3;
	Scanner entrada = new Scanner(System.in);


	public static void main(String[] args) {
		MiPractica1 obj = new MiPractica1();
		obj.acciones();
		
	}
	
	public void acciones() {
		acciones1();
		acciones2();
		acciones3();
	}
	
	public void acciones1() {
		System.out.println("--ACCIONES 1--");
		rectangulo1= new Rectangulo();
		rectangulo2= new Rectangulo(5,15);
		rectangulo3= new Rectangulo(4,8,2,3);
		System.out.println(rectangulo1.toString());
		System.out.println(rectangulo2.toString());
		System.out.println(rectangulo3.toString());
	}

	public void acciones2() {
		System.out.println("==ACCIONES 2==");
		rectangulo1 = new Rectangulo();
		rectangulo2 = new Rectangulo();
		rectangulo3 = new Rectangulo();
		rectangulo2.setX(5);
		rectangulo2.setY(15);
		rectangulo2.setAlto(1);
		rectangulo2.setAncho(1);
		rectangulo3.setX(4);
		rectangulo3.setY(8);
		rectangulo3.setAlto(2);
		rectangulo3.setAncho(3);
		System.out.println(rectangulo1.toString());
		System.out.println(rectangulo2.toString());
		System.out.println(rectangulo3.toString());
	}

	public void acciones3() {
		System.out.println("**ACCIONES 3**");
		rectangulo1 = new Rectangulo();
		rectangulo2 = new Rectangulo();
		rectangulo3 = new Rectangulo();
		System.out.println("Introduce datos:");
		rectangulo2.setX(entrada.nextFloat());
		rectangulo2.setY(entrada.nextFloat());
		rectangulo2.setAlto(entrada.nextFloat());
		rectangulo2.setAncho(entrada.nextFloat());
		rectangulo3.setX(entrada.nextFloat());
		rectangulo3.setY(entrada.nextFloat());
		rectangulo3.setAlto(entrada.nextFloat());
		rectangulo3.setAncho(entrada.nextFloat());
		System.out.println(rectangulo1.toString());
		System.out.println(rectangulo2.toString());
		System.out.println(rectangulo3.toString());
		
	}

}
