package laboratorio1;

import static org.junit.Assert.*;

import org.junit.Test;

public class RectanguloTest {

	@Test
	public void testArea() {
		assertEquals(8 , new Rectangulo(1,1,2,4).calcularArea(),0);
		//los dos primeros parametros del constructos son la X y la Y
	}
 
	@Test
	public void testGetters() {
		assertEquals(0 , new Rectangulo().getX(),0);
		assertEquals(0 , new Rectangulo().getY(),0);
		assertEquals(0 , new Rectangulo().getAlto(),0);
		assertEquals(0 , new Rectangulo().getAncho(),0);

	}

}
