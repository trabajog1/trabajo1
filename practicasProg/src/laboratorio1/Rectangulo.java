/**
 * 
 */
package laboratorio1;

/**
 * @author MiguelQuevedo
 *
 */
public class Rectangulo {
	
	private float x , y;
	private float ancho , alto;
	
	public Rectangulo() {
		x=y=0;
		ancho=alto=0;
	}
	
	public Rectangulo(float x, float y) {
		this.x=x;
		this.y=y;
		alto=ancho=1;
	}
	
	public Rectangulo(float x, float y,float alto , float ancho) {
		this.x=x;
		this.y=y;
		this.alto=alto;
		this.ancho=ancho;	
	}
	
	

	/**
	 * devuelve la x
	 * @return la x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * devuelve la y
	 * @return la y
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * devuelve el ancho
	 * @return el ancho
	 */
	public float getAncho() {
		return ancho;
	}

	/**
	 * @param ancho the ancho to set
	 */
	public void setAncho(float ancho) {
		this.ancho = ancho;
	}

	/**
	 * devuelve el alto 
	 * @return el alto
	 */
	public float getAlto() {
		return alto;
	}

	/**
	 * @param alto the alto to set
	 */
	public void setAlto(float alto) {
		this.alto = alto;
	}
	
	/**
	 * calcula el area del rectangulo en base del alto y ancho
	 * @return el area del rectangulo
	 */
	public float calcularArea() {
		return alto*ancho;
	}

	@Override
	public String toString() {
		return "Rectangulo en (" + x + "," + y + ") con un area de " + calcularArea() + "";
	}

}
