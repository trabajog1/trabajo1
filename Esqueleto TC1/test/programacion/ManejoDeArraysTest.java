package programacion;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Before;

import datos.Ciudad;
import datos.Pais;



public class ManejoDeArraysTest {
	
	private ArrayList<Pais> continente; 
	private Pais[] paises;
	private Pais pais1, pais2, pais3, pais4, pais5;
	private Ciudad ciudad1, ciudad2, ciudad3, ciudad4, ciudad5, ciudad6, ciudad7, ciudad8, ciudad9, ciudad10; 

	private float[] rellenarTemperatura(int indice) {
		float[] temperaturas = new float[24];
		Random aleatorio = new Random(System.currentTimeMillis() * indice);
		for (int i = 0; i < temperaturas.length; i++) {
			if (aleatorio.nextBoolean()) 
				temperaturas[i] = aleatorio.nextInt(40);
			else
				temperaturas[i] = -aleatorio.nextInt(15);
		}
		return temperaturas;
	}
	
	@Before
	public void setUp() throws Exception {
		// ejemplo de posibles datos de pruebas. Los estudiantes pueden y deben hacer
		// sus propios datos de pruebas para probar los distintos casos que se puedan dar en
		// los m�todos que vayan a probar. Estos datos pueden cambiar a valores espec�ficos que
		// tengan que ver con el problema
		// LOS DATOS DE TEMPERATURA NO SON REALES, NI PARECIDOS A LOS QUE PODR�A TENER UNA
		// CIUDAD. SON DATOS GENERADOS ALEATORIAMENTE	
		float[] temperaturas1 = rellenarTemperatura(1);
		float[] temperaturas2 = rellenarTemperatura(2);
		float[] temperaturas3 = rellenarTemperatura(3);
		float[] temperaturas4 = rellenarTemperatura(4);
		float[] temperaturas5 = rellenarTemperatura(5);
		float[] temperaturas6 = rellenarTemperatura(6);
		float[] temperaturas7 = rellenarTemperatura(7);
		float[] temperaturas8 = rellenarTemperatura(8);
		float[] temperaturas9 = rellenarTemperatura(9);
		float[] temperaturas10 = rellenarTemperatura(10);
		ciudad1 = new Ciudad("Ciudad 1", temperaturas1,  40000, true, 1234.5f);
		ciudad2 = new Ciudad("Ciudad 2", temperaturas2,  140000, false, 2348.75f);
		ciudad3 = new Ciudad("Ciudad 3", temperaturas3,  1040000, false, 12348.75f);
		ciudad4 = new Ciudad("Ciudad 4", temperaturas4,  1100, true, 348.75f);
		ciudad5 = new Ciudad("Ciudad 5", temperaturas5,  20500100, true, 112348.75f);
		ciudad6 = new Ciudad("Ciudad 6", temperaturas6,  500100, false, 234893.5f);
		ciudad7 = new Ciudad("Ciudad 7", temperaturas7,  50300, true, 34893.5f);
		ciudad8 = new Ciudad("Ciudad 8", temperaturas8,  100, false, 893.5f);
		ciudad9 = new Ciudad("Ciudad 9", temperaturas9,  100789, true, 12893.59f);
		ciudad10 = new Ciudad("Ciudad 10", temperaturas10,  55100, false, 25893.5f);
		pais1 = new Pais("Pais 1",  
				new Ciudad[]{ciudad1, ciudad3, null, null, null, null, null, null, null, null}, 2, 'N'); 
		pais2 = new Pais("Pais 2", 
				new Ciudad[]{ciudad2, ciudad5, ciudad4, null, null, null, null, null, null, null}, 3, 'N'); 
		pais3 = new Pais("Pais 3",   
				new Ciudad[]{ciudad6, null, null, null, null, null, null, null, null, null}, 1, 'S'); 
		pais4 = new Pais("Pais 4",   
				new Ciudad[]{ciudad7, ciudad8, null, null, null, null, null, null, null, null}, 2, 'N'); 
		pais5 = new Pais("Pais 5", 
				new Ciudad[]{ciudad9, ciudad10, null, null, null, null, null, null}, 2, 'S'); 
		
		continente = new ArrayList<Pais>();
		continente.add(pais1);
		continente.add(pais2);
		continente.add(pais3);
		continente.add(pais4);
		continente.add(pais5);
		
		paises = new Pais[10];
		paises[0] = pais1;
		paises[1] = pais2;
		paises[2] = pais3;
		paises[3] = pais4;
		paises[4] = pais5;
		// las posiciones del array sin paises, deben estar a null
		paises[5] = paises[6] = paises[7] = paises[8] = paises[9] = null;
	}


	// SE RECOMIENDA QUE SE ASIGNEN VALORES REALES PARA PODER COMPROBAR LOS M�TODOS
	
	// Aqu� van todos los tests del PRIMER ejercicio del TC1
	
	
	// Este test comprueba si los 5 primeros elementos del array paises
	// (que fij�ndonos m�s arriba tienen asignados 1 pa�s cada uno, es decir
	// pais1, pais2, pais3, pais4 y pais5) tienen costa (true) o no (false)
	// introduciendo el nombre del pais (p. ej. "Pais 1") como par�metro del
	// m�todo tieneCosta(), obteniendo su resultado mediante los assertTrue 
	// (si esperamos que sea true) o assertFalse (si esperamos que sea false)
	@Test
	public void testTieneCosta() throws ValorNulo, NoRquisito {
		// Creaci�n de un objeto de la clase donde hemos trabajado
		ManejoDeArrays mda=new ManejoDeArrays(paises, paises);
		// Comprobaci�n del m�todo tieneCosta
		assertTrue(mda.tieneCosta("Pais 1"));
		assertTrue(mda.tieneCosta("Pais 2"));
		// pais3 NoRquisito
		assertTrue(mda.tieneCosta("Pais 4"));
		// pais5 NoRequisito
	}
	
	
	// Aqu� van todos los tests del SEGUNDO ejercicio del TC1

		@Test(expected=ValorVacio.class)
		public void testordnarcontinentevacios () throws Exception {
			ManejoDeArrays pais = new ManejoDeArrays(new Pais[] {} , paises);
			pais.ordenarPaises();
	}
		@Test
		public void testordnarcontinentevacios1 ()  {
			ManejoDeArrays pais = new ManejoDeArrays(new Pais[] {} , paises);
			continente = new ArrayList<Pais>();
			continente.add(pais1);
			continente.add(pais2);
			continente.add(pais3);
			continente.add(pais4);
			continente.add(pais5);
			pais.ordenarMenorAMayor();
			
		}
	// Aqu� van todos los tests del TERCER ejercicio del TC1
    @Test
    public void tesCalcularPoblacionSupLimitee() throws Exception {
	    // en vez de que pais[5] sea null lo igualo a un valor temporal creado de tipo Pais 
    	float[] temperaturas11 = rellenarTemperatura(11);
    	float[] temperaturas12 = rellenarTemperatura(12); 
    	Ciudad ciudad11 = new Ciudad("Ciudad 11",  temperaturas11,  743200, false, 274398.5f);
    	Ciudad ciudad12 = new Ciudad("Ciudad 12", temperaturas12,  2314 , true, 8743.5f);
    	Pais pais6 = new Pais("Pais 6",new Ciudad[]{ciudad11, ciudad12, null, null, null, null, null, null, null, null}, 2, 'N'); 
    	paises[5] = pais6;
    	
    	ManejoDeArrays paisContinente = new ManejoDeArrays(paises, paises);
    	int poblacionTotal = paisContinente.calcularPoblacion("Pais 6", 1000);
	
	    // como poblacionTotal es mayor al limite devuelo el valor de la poblacion 
    	assertEquals( 745514 , poblacionTotal);
	}
    
    @Test(expected=PaisNoExiste.class)
	public void testPaisNoExiste() throws Exception {
		ManejoDeArrays paisContinente = new ManejoDeArrays(paises, paises);
		paisContinente.calcularPoblacion("Pais 2654", 1000);
	}  

		
	// Aqu� van todos los tests del CUARTO ejercicio del TC1
    /**
     *@author Alvaro Pe�alver Valverde
     */
    @Test  
	public void testTemperatura1()  {
		//Setup del test
		float[] temperaturasControladas = new float[]{10f, 11f, 12f, 13f, 14f, 15f, 16f, 17f , 18f, 19f, 20f, 21f, 22f, 23f,
		24f, 25f, 26f, 27f, 28f, 29f, 30f, 31f, 32f, 33f, 34f};		//Valores controlados para probar el metodo
		ciudad1 = new Ciudad("Ciudad 1", rellenarTemperatura(1),  40000, true, 4234.5f);
		ciudad2 = new Ciudad("Ciudad 2", rellenarTemperatura(2),  140000, false, 5448.75f);
		ciudad3 = new Ciudad("Ciudad 3", rellenarTemperatura(3),  1040000, false, 23248.75f);
		ciudad4 = new Ciudad("Ciudad 4", rellenarTemperatura(4),  1100, false, 3481.75f);
		ciudad5 = new Ciudad("Ciudad 5", temperaturasControladas,  20500100, true, 11348.75f);
		ciudad6 = new Ciudad("Ciudad 6", rellenarTemperatura(6),  525100, true, 8248.32f);
		pais1 = new Pais("Pais 1", new Ciudad[]{ciudad1, ciudad2, null, null, null, null, null, null, null, null}, 2, 'N');
		pais2 = new Pais("Pais 2", new Ciudad[]{null, null, null, null, null, null, null, null, null, null}, 0, 'S');
		pais3 = new Pais("Pais 3", new Ciudad[]{ciudad3, ciudad4, ciudad5, ciudad6, null, null, null, null, null, null}, 4, 'S');
		ManejoDeArrays m = new ManejoDeArrays(new Pais[] {pais1, pais2, pais3}, new Pais[] {});
		//Test	
		try {
			assertTrue(m.buscarMayorTemperaturaCostera("Pais 3").equals("10.0 34.0"));
		}catch(Exception error){
			error.printStackTrace();
		}
	}
    /**
     *@author Juan Carlos Medina
     */
	@Test
	public void testTemperatura2()  {
		//Setup del test
		ciudad1 = new Ciudad("Ciudad 1", rellenarTemperatura(1),  40000, false, 12334.5f);
		ciudad2 = new Ciudad("Ciudad 2", rellenarTemperatura(2),  12000, false, 9852.5f);
		ciudad3 = new Ciudad("Ciudad 3", rellenarTemperatura(3),  32000, false, 2137.5f);
		pais1 = new Pais("Pais 1", new Ciudad[]{ciudad1, ciudad2, ciudad3, null, null, null, null, null, null, null}, 3, 'S');
		ManejoDeArrays m = new ManejoDeArrays(new Pais[] {pais1}, new Pais[] {});
		//Test	
		try {
			assertTrue(m.buscarMayorTemperaturaCostera("Pais 1") == null);
		}catch(Exception error){
			error.printStackTrace();
		}
	}
		
	// Aqu� van todos los tests del QUINTO ejercicio del TC1
	

	@Test
	public void testA�adir() throws Exception  {
		Pais paisNuevo = new Pais("Jerusalen", new Ciudad[]{ciudad9, ciudad10, null, null, null, null, null, null}, 5, 'N');
		ManejoDeArrays m = new ManejoDeArrays(paises, paises) ;
		assertTrue(m.insertarPais(paisNuevo, 2));
		assertEquals(paises[0],pais1);
		assertEquals(paises[1],pais2);
		assertEquals(paises[2],paisNuevo);
		assertEquals(paises[3],pais3);
		assertEquals(paises[4],pais4);
		assertEquals(paises[5],pais5);	
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testIndexOutOfBoundsException() throws Exception{
		Pais paisNuevo = new Pais("Jerusalen", new Ciudad[]{ciudad9, ciudad10, null, null, null, null, null, null}, 5, 'N');
		ManejoDeArrays m = new ManejoDeArrays(paises, paises);
		m.insertarPais(paisNuevo, 15);
	}
	
		
	// Aqu� van todos los tests del SEXTO ejercicio del TC1
	@Test
	public void testEliminarPais1() throws Exception{
		//Creamos un nuevo pais llamado Espa�a
		Pais pais6 = new Pais("Espa�a",new Ciudad[]{ciudad8, ciudad7, ciudad6,ciudad4, ciudad5, null, null, null, null, null}, 5, 'N'); 
    	paises[5] = pais6;
		//Creacion objeto de la clase ManejoDeArrays
    	ManejoDeArrays pais = new ManejoDeArrays(paises,paises) ;
		//Comprobamos que Espa�a se elimina del array
		assertEquals(true, pais.eliminarElemento("Espa�a", 6));
		//Comprobamos que los dem�s pa�ses no se eliminan
		assertEquals(paises[0], pais1);
		assertEquals(paises[1], pais2);
		assertEquals(paises[2], pais3);
		assertEquals(paises[3], pais4);
		assertEquals(paises[4], pais5);
	}
	
	@Test(expected=PaisNoExiste.class)
	 public void testPaisNoExiste6()throws Exception {
		
		ManejoDeArrays paisEliminado = new ManejoDeArrays(paises,paises) ;
		paisEliminado.eliminarElemento("Pais 11", 3);
		
		
	}

}
