package datos;

import java.util.Arrays;

public class Ciudad {

	private String nombre; // nombre de la ciudad
	private float[] temperaturas; // conjunto de temperaturas de la ciudad (en las 24 horas de un d�a)
	private int poblacion; // n�mero de habitantes de la ciudad
	private boolean costera; // indica si la ciudad tiene costa (est� junto al mar)
	private float area; // �rea de la ciudad
	
	public Ciudad(String nombre, float[] temperaturas, int poblacion, boolean costera, float area) {
		super();
		this.nombre = nombre;
		this.temperaturas = temperaturas;
		this.poblacion = poblacion;
		this.costera = costera;
		this.area = area;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float[] getTemperaturas() {
		return temperaturas;
	}

	public void setTemperaturas(float[] temperaturas) {
		this.temperaturas = temperaturas;
	}

	public int getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}

	public boolean isCostera() {
		return costera;
	}

	public void setCostera(boolean costera) {
		this.costera = costera;
	}

	public float getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(area);
		result = prime * result + (costera ? 1231 : 1237);
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + poblacion;
		result = prime * result + Arrays.hashCode(temperaturas);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ciudad other = (Ciudad) obj;
		if (Float.floatToIntBits(area) != Float.floatToIntBits(other.area))
			return false;
		if (costera != other.costera)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (poblacion != other.poblacion)
			return false;
		if (!Arrays.equals(temperaturas, other.temperaturas))
			return false;
		return true;
	}
	
	
}
