package datos;

import java.util.Arrays;

public class Pais {
	private String nombre; // nombre del pa�s
	private Ciudad[] ciudades; // lista de ciudades del pa�s
	private int nCiudades; // n�mero de elementos del array ciudades
	private char hemisferio; // si es 'N' el pa�s est� en el hermisferio norte y si es 'S' est� en el sur
	
	public Pais(String nombre, Ciudad[] ciudades, int nCiudades, char hemisferio) {
		super();
		this.nombre = nombre;
		this.ciudades = ciudades;
		this.nCiudades = nCiudades;
		this.hemisferio = hemisferio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Ciudad[] getCiudades() {
		return ciudades;
	}

	public void setCiudades(Ciudad[] ciudades) {
		this.ciudades = ciudades;
	}

	public int getnCiudades() {
		return nCiudades;
	}

	public void setnCiudades(int nCiudades) {
		this.nCiudades = nCiudades;
	}

	public char getHemisferio() {
		return hemisferio;
	}

	public void setHemisferio(char hemisferio) {
		this.hemisferio = hemisferio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(ciudades);
		result = prime * result + hemisferio;
		result = prime * result + nCiudades;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		if (!Arrays.equals(ciudades, other.ciudades))
			return false;
		if (hemisferio != other.hemisferio)
			return false;
		if (nCiudades != other.nCiudades)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
	
}
