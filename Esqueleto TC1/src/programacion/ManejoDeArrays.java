package programacion;

import java.util.ArrayList;

import datos.Ciudad;
import datos.Pais;

public class ManejoDeArrays {
	

	// En el atributo continente se almacena la informaci�n de una serie de paises (nombre, 
	// ciudades y en qu� hemisferio est�) que forman parte del continente
	private ArrayList<Pais> continente;
		
	// En el atributo paises se almacenan algunos paises que forman parte de un continente cualquiera 
	private Pais[] paises; // se debe usar un array de longitud 10
	private int nPaises; // n�mero de elementos en el array paises
	
	// Crea el ArrayList continente usando la informaci�n del primer par�metro conjuntoPaisesDelArrayList
	// Crea el array paises usando la informaci�n del segundo par�metro conjuntoPaisesDelArray
	public ManejoDeArrays(Pais[] conjuntoPaisesDelArrayList, Pais[] conjuntoPaisesDelArray) {
		continente = new ArrayList<Pais>();
		if (conjuntoPaisesDelArrayList != null) {
			int i = 0;
			while (i < conjuntoPaisesDelArrayList.length && conjuntoPaisesDelArrayList[i] != null) {
				continente.add(conjuntoPaisesDelArrayList[i]);
				i++;
			}
		}
		paises = conjuntoPaisesDelArray;
		nPaises = 0;
		// si paises es null, se crea un array de paises, de tama�o 10, vac�o
		if (paises == null) {
			paises = new Pais[10];
			return;
		}
		// nPaises s�lo debe almacenar el n�mero de elementos del array que no sean null
		while (nPaises < paises.length && paises[nPaises] != null)
			nPaises++;
	}
		
	public int getNPaises() {
		return nPaises;
	}
		
	public Pais getPaisEnIndice(int indice) {
		return paises[indice];
	}
		
	public ArrayList<Pais> getContinente() {
		return continente;
	}
	
	
	/*****************************************************************************************
	 * IMPORTANTE
	 * 
	 * ANTES DE EMPEZAR A HACER LOS EJERCICIOS, ANALIZAR LAS CLASES Ciudad y Pais del paquete 
	 * datos Y LOS ATRIBUTOS Y EL CONSTRUCTOR DE LA CLASE ManejoDeArrays. NO OLVIDAR QUE HAY
	 * QUE LEER BIEN LA ESPECIFICACI�N (ARCHIVO PDF) DE LA EXPLICACI�N DEL TRABAJO QUE SE
	 * ENCUENTRA EN CAMPUS VIRTUAL. 
	 * 
	 *****************************************************************************************/
	
	//M�todos comunes
	
	public Pais[] desplazarDcha(int inicio, int acabar){
		if(inicio>=paises.length) {
			return paises;
		}if(acabar>paises.length) {
			return paises;
		}
		for(int k=acabar;k>=inicio;k--) {
			paises[k]=paises[k-1];
		}
		return paises;	
	}
	
	public Pais[] desplazarIzq(int inicio, int acabar) {
		if(inicio>=paises.length) {
			return paises;
		}if(acabar>paises.length) {
			return paises;
		}
		for(int k=inicio+1;k<=acabar;k++) {
			paises[k-1]=paises[k];
		}
		return paises;	
	}
	
	
	// TODO PRIMER ejercicio del TC1
	/**
	* M�todo (llamado tieneCosta) que dado el nombre de un pa�s (usar el array paises), indique 
	* si el pa�s tiene costa o no, siempre que est� en el hemisferio norte. Un pa�s tiene costa 
	* si alguna de sus ciudades tiene costa. 
	* @param pais Nombre del pais a buscar.
	* @return True si tiene costa y falso si no tiene costa o el pa�s no existe. 
	* @throws ValorNulo Si pais es null.
	* @throws NoRequisito Si el pa�s est� en el array, pero no est� en el hemisferio norte.
	*/
	
	public boolean tieneCosta(String pais) throws ValorNulo, NoRquisito {
		//Excepcion ValorNulo
		if(pais==null) {
			throw new ValorNulo();
		}
		// Recorremos los elementos del array paises, obtenemos el nombre de
		// cada pa�s con el getter y si coincide con el nombre del par�metro
		// comprobamos si este se encuentra en el hemisferio Norte
		for(int i=0;i<nPaises;i++) {
			if(paises[i].getNombre().equals(pais)) {
				if(paises[i].getHemisferio()=='N')	{
					// si est� en el hemisferio 'N', obtenemos el array ciudades
					// del pa�s que cumple las condiciones anteriores, guardando 
					// en una variable el array ciudades de dicho pa�s y el n�mero
					// de ciudades que tiene
					Ciudad[] ciud=paises[i].getCiudades();
					int nCiud=paises[i].getnCiudades();
					// recorremos el array ciudades y obtenemos el valor del getter
					// isCostera. Si es true, la ciudad tiene costa y por tanto su 
					// pa�s tambi�n; si salimos del bucle sin obtener un resultado
					// true quiere decir que no se ha encontrado ninguna ciudad con
					// costa, por lo tanto devuelve false
					for(int k=0;k<nCiud;k++) {
						if(ciud[k].isCostera()==true) {
							return true;
						}
					}
					return false;
				// si no se cumple la condici�n de que su hemisferio sea 'N', pero
				// s� la anterior (de que el nombre se encuentra en el array paises)
				// lanza la excepci�n NoRquisito
				} else {
					throw new NoRquisito();
				}
			}
		}
		// Devuelve false en cualquier otro caso distinto de lo planteado
		return false;
	}

	
	// TODO SEGUNDO ejercicio del TC1
	/**
	* M�todo (llamado ordenarPaises) que ordena la colecci�n continente siguiendo el siguiente criterio: de menor 
	* a mayor por el hemisferio en el que est� el pa�s. A igual de hemisferio, de menor a mayor por el nombre del 
	* pa�s. 
	* @throws ValorNulo Si continente es null.
	* @throws ValorVacio Si continente est� vac�o.
	*/	

	public void ordenarPaises() throws Exception {
		if (continente == null) {//Excepcion valornulo
			throw new ValorNulo();	
		}
		else if (continente.size() == 0) {//Excepcion si continente esta vacio
			throw new ValorVacio();
		}
	}
	//Para poder hacer la ordenacion tenemos que hacer dos metodos nuevos.
	//Uno para buscar la posicion del menor en la coleccion continente y luego otro en el que se llame a este
	//metodo que lo he llamado ordenar para que sea mas sencilla su ordenacion
		public int busquedaMenor(int i) {// busquedadelmenor
		Pais menor = continente.get(i);
		int posiciondelmenor = 0;
		for(int j=i+1;j<continente.size();j++) {
			Pais pais = continente.get(j);
			if(pais.getHemisferio() < menor.getHemisferio()){
				menor = pais;
				posiciondelmenor = j;
			}
			if(pais.getHemisferio() == menor.getHemisferio()) {
				if(pais.getNombre().compareTo(menor.getNombre())<0) {
					menor = pais;
					posiciondelmenor = j;
				}
			}
		}
		return posiciondelmenor;
	}
	public void ordenarMenorAMayor() {//ordenar de menor a mayor
		for (int i = 0 ; i < continente.size() ; i++) {
			// busca el menor desde la posici�n i
			int posiciondelmenor = busquedaMenor(i); 
			// intercambio de elementos dentro del array
			// i <-> posicionDelMenor
			Pais temp = continente.get(i); 
			continente.add(i,continente.get(posiciondelmenor)) ;
			continente.add(posiciondelmenor,temp);
			}
		}

	// TODO TERCER ejercicio del TC1
	/**
	* M�todo (llamado calcularPoblacion) que dado el nombre de un pa�s (usar la colecci�n continente), 
	* indique si la poblaci�n de ese pa�s supera un valor l�mite. La poblaci�n de un pa�s es la suma
	* de las poblaciones de las ciudades que lo forman.
	* @param pais Nombre del pais a buscar.
	* @param limite L�mite de poblaci�n con el que comparar.
	* @return El n�mero de habitantes total (poblaci�n) o -1 si la poblaci�n total no supera el l�mite.
	* @throws ValorNulo Si pais es null.
	* @throws PaisNoExiste Si el pa�s no est� en el ArrayList continente.
	*/
	public int calcularPoblacion(String pais, int limite) throws ValorNulo , PaisNoExiste { // Encabezado del ejercicio 
		
		// si pais es null elevamos la excepcion 
			if (pais == null) {
				throw new ValorNulo();

			} 
	        // creamos un contador donde iremos guardando la poblacion de las ciudades de los paises que se encuentran en el continente 
				int poblacionTotalPais = 0;

				for (int i = 0; i < continente.size(); i++) {
					if (continente.get(i).getNombre().equals(pais)) {
						Ciudad[] ciudades = continente.get(i).getCiudades();
						int nCiudades = continente.get(i).getnCiudades();
						for (int k = 0; k <  nCiudades ; k++) {

							int poblacion = ciudades[k].getPoblacion();
							poblacionTotalPais = poblacionTotalPais + poblacion;
	         // decido que devuelvo en funcion de las condiciones 
						}
						if (poblacionTotalPais >= limite) {
							return poblacionTotalPais;

						} else {
							return -1;
						}

					}

				}
	           // si no encuentro el pais en el continente devuelvo PaisNoExiste 
				throw new PaisNoExiste();
			
			 
		}

	
	
	
	// TODO CUARTO ejercicio del TC1
	/**
	* M�todo (llamado buscarMayorTemperaturaCostera) que, para un pais dado (usar la colecci�n continente),
	* busque la temperatura mayor y la temperatura menor de la primera ciudad costera que encuentre en el 
	* array ciudades de ese pa�s.
	* Nota: se recomienda que haga un m�todo para buscar el mayor (buscarMayor) que devuelva la temperatura
	* mayor y otro para buscar el menor (buscarMenor), que devuelva la temperatura menor y los llame desde
	* el m�todo buscarMayorTemperaturaCostera. Ambos m�todos deben tener por par�metros, el array temperaturas
	* en el que se busca el mayor o el menor.
	* @param pais Nombre del pais a buscar.
	* @return Una String con la temperatura menor un espacio y la temperatura mayor encontradas. Por ejemplo,
	* si la temperatura menor es 8 y la temperatura mayor es 24, la String devuelta es "8 24". Si el pais
	* no est� o no tiene ciudad costera, se devuelve null.
	* @throws ValorNulo Si pais es null.
	* @throws ValorVacio Si pais est� vac�o (es decir, no tiene un valor almacenado)
	*/
	public String buscarMayorTemperaturaCostera(String pais) throws Exception {
		if(pais == null) throw new ValorNulo(); //Si el pais es null
		if(pais.equals("")) throw new ValorVacio(); //Si el pais esta vacio
		for(int i = 0; i < continente.size(); i++) {
			if(continente.get(i).getNombre().equals(pais)) {
				Ciudad[] ciudades = continente.get(i).getCiudades();							
				for(int index = 0; index < continente.get(i).getnCiudades(); index++){
					if(ciudades[index].isCostera()) {
						return buscarMenor(ciudades[index].getTemperaturas()) + " " + buscarMayor(ciudades[index].getTemperaturas());					
					}				
				}
			}
		}
		return null;
	}
	public float buscarMayor(float[] data) {
		float curentHighest = data[0];
		for(int i = 0; i < data.length; i++) {
			if(curentHighest < data[i]) {
				curentHighest = data[i];
			}
		}
		return curentHighest;
	}
	public float buscarMenor(float[] data) {
		float curentLowest = data[0];
		for(int i = 0; i < data.length; i++) {
			if(curentLowest > data[i]) {
				curentLowest = data[i];
			}
		}
		return curentLowest;
	}
	
	
	
	// TODO QUINTO ejercicio del TC1
	/**
	* M�todo (llamado insertarPais) que inserte un nuevo pais en el array paises, si no est�.
	* @param pais El pais (objeto Pais) a insertar en el array.
	* @param indice Posici�n donde se inserta el pa�s en el array, si no est�.
	* @return Si se pudo o no insertar.
	* @throws ValorNulo Si pais es null.
	* @trhows EstaLleno Si el array paises est� lleno.
	* @throws IndexOutOfBoundsException Si indice no est� en el rango 0 <= indice <= nPaises.
	*/
	
	public boolean insertarPais(Pais pais,int indice) throws Exception  {
		if(indice>paises.length||indice<0) { // Si la posici�n no esta en el array
			throw new IndexOutOfBoundsException();	
		}
		if(paises.length==nPaises) {//Si el numero de paises es igual al tama�o del array es que esta lleno
			throw new EstaLleno();			
		}
		if(pais==null) {//Si es el pais que se le pasa es nulo se tira la exepcion
			throw new ValorNulo();			
		}
		int estaElPais=0;
		for(int k=0;k<nPaises;k++) {//Si la cuenta es 1 el pais ya esta en el array
			if(paises[k].getNombre().equals(pais.getNombre()))
				estaElPais=1;
		}
		if(estaElPais==0) { //Si el pais esta no incluye nada
			if(indice==nPaises) {
				paises[indice]=pais;
				nPaises++;
				return true;
			}else if(indice<=nPaises){
				paises = desplazarDcha(indice,nPaises);
				paises[indice]=pais;
				nPaises++;
				return true;
			}
		}
			return false;
		
	}
	
	
	
	// TODO SEXTO ejercicio del TC1
	/**
	* M�todo (llamado eliminarPais) que elimine un pais de nombre dado si tiene menos de n ciudades   
	* (usando el array paises). Tener en cuenta que el pais, si est�, puede estar al principio, al final o 
	* en otra posici�n distinta a las anteriores.
	* @param pais El nombre del pais.
	* @param n Valor entero.
	* @return Si se elimin� o no el pa�s.
	* @throws ValorNulo Si pais es null.
	* @throws ValorVacio Si pais no contiene un valor.
	* @throws PaisNoExiste Si el pais no est� en el array paises.
	* 
	*/
	public boolean eliminarElemento(String pais, int n) throws ValorNulo, PaisNoExiste, ValorVacio {

		if (pais == null) {         // Excepcion ValorNulo porque pais es null.

			throw new ValorNulo();

		} else if (pais.equals("")) {  // Excepcion ValorVacio,no le hemos asignado al pais ning�n valor.

			throw new ValorVacio();

		} else {
			
			if (nPaises != 0) {
				for (int k = 0; k < nPaises; k++) {
					if (pais.equals(paises[k].getNombre())) {
						if (paises[k].getnCiudades() < n) {           // condicion si n es mayor que el n�de ciudades del pais,
															          // eliminamos
							if (k != nPaises - 1) {                   // condicion, no se encuentra en la �ltima posici�n, desplazamos
		           								                      // hacia la izquierda y se reduce longitud para eliminar.
								desplazarIzq(k, nPaises--);
								nPaises--;
								return true;         // devuelve true se ha eliminado el pais.
							
							} else {                 // si se encuentra en la ultima posicion.
								nPaises--;
								return true;        // devuelve true se ha eliminado.
							} 
						}
						return false; 
					}
				}
				throw new PaisNoExiste(); // Excepcion PaisNoExiste porque el pais no
				                          // se encuentra al salir del bucle for.
			}
		}                                 
		return false;   //Devuelve falso, no se ha podido eliminar pais porque nPaises es cero
	}


	
	
	
}
